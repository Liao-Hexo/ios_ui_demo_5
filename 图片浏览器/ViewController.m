//
//  ViewController.m
//  图片浏览器
//
//  Created by 廖家龙 on 2020/4/21.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

//声明picture属性为了重写它的getter方法，懒加载数据
@property(nonatomic,strong)NSArray *picture;

//自己写一个索引，来控制当前显示的是第几张图片，这个属性一开始没有赋值就是0
@property(nonatomic,assign) int index;

@property (weak, nonatomic) IBOutlet UILabel *suoyin;
@property (weak, nonatomic) IBOutlet UIImageView *tupian;
@property (weak, nonatomic) IBOutlet UILabel *biaoti;

- (IBAction)last;//点击上一张箭头
- (IBAction)next;//点击下一张箭头

//设置"上一张"“下一张”按钮是否可以点击
@property (weak, nonatomic) IBOutlet UIButton *lastButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation ViewController

//重写picture属性的getter方法（懒加载数据）
- (NSArray *)picture{
    if(_picture==nil){
        
        //写代码加载picture.plist文件中的数据到_picture
        
        //1.获取picture.plist文件的路径赋值给path变量，[NSBundle mainBundle]表示获取这个app安装到手机时的根目录，然后在app安装的根目录下搜索picture.plist文件的路径
        NSString *path=[[NSBundle mainBundle] pathForResource:@"picture.plist" ofType:nil];
        //2.读取文件
        NSArray *array=[NSArray arrayWithContentsOfFile:path];
        _picture=array;
    }
    return _picture;
}

//控制器的view加载完毕以后执行的方法
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.index=-1;

    //这行代码的作用：控制器view加载完毕之后，执行‘下一张图片’的方法，index=0，再执行setData方法来设置控件的数据，将picture字典中的数据加载进来
    [self next];
    
    //手动写plist代码，如果要永久的保存数据，就要创建plist文件
//    NSDictionary *dict1=@{@"name" : @"张三", @"age" : @18, @"height" : @180};
//    NSDictionary *dict2=@{@"name" : @"李四", @"age" : @19, @"height" : @190};
//    NSDictionary *dict3=@{@"name" : @"王五", @"age" : @17, @"height" : @178};
//    NSArray *students=@[dict1,dict2,dict3];
//    NSLog(@"%@",students);//打印出数据

}

//上一张图片
- (IBAction)last {
    
    self.index--;//让索引--
    
    [self setData];//设置控件数据
}

//下一张图片
- (IBAction)next {
    
    self.index++;//让索引++
    
    [self setData];//设置控件数据
}

- (void)setData{
    
    //1.从数组中获取当前这张图片的数据
    NSDictionary *dict=self.picture[self.index];
    
    //2.把获取到的数据设置给界面上的控件
    
       //设置索引
    self.suoyin.text=[NSString stringWithFormat:@"%d/%ld",self.index+1,self.picture.count];
       //通过image属性来设置图片框里面的图片
    self.tupian.image=[UIImage imageNamed:dict[@"图片名称"]];
       //设置这张图片的标题
    self.biaoti.text=dict[@"图片标题"];
    
    //3.设置"上一张"“下一张”按钮是否可以点击
    self.lastButton.enabled=(self.index != 0);
    self.nextButton.enabled=(self.index != (self.picture.count-1));
}

@end
